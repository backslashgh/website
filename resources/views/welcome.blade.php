<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.websolution.w3codemasters.in/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Jan 2019 03:52:25 GMT -->
<head>
<title>Websolution</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="images/favicon.jpg">
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="css/animate.css" type="text/css">
<link rel="stylesheet" href="css/swipebox.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
<!-- NAVIGATION -->
<nav class="navbar navbar-inverse wow fadeInDown" data-wow-duration="0.4s" data-wow-delay="1s">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="#"><img src="images/logo.png"></a> </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#myCarousel"><i class="fa fa-circle-o"></i> Home</a></li>
        <li ><a href="#about"><i class="fa fa-circle-o"></i>about</a></li>
        <li ><a href="#services"><i class="fa fa-circle-o"></i>services</a></li>
        <li ><a href="#portfolio"><i class="fa fa-circle-o"></i>portfolio</a></li>
        <li ><a href="#"><i class="fa fa-circle-o"></i>blog</a></li>
        <li ><a href="#contact"><i class="fa fa-circle-o"></i>contact us</a></li>
      </ul>
    </div>
  </div>
</nav>
<!-- SLIDER -->
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="8000" data-pause="false">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <div class="carousel-caption"> <span> WELCOME TO</span>
        <h3>IT WORLD</h3>
        <p><span>Web Design</span>, <span>Development</span> & <span>Branding </span></p>
        <a href="#" class="get"> get started</a> </div>
      <img src="images/slider1.jpg" alt="slider" class="img-responsive"> </div>
    <div class="item">
      <div class="carousel-caption"> <span> malesuada scelerisque</span>
        <h3>ullamcorper </h3>
        <p><span>molestie</span>, <span>semper </span> & <span>pellentesque </span></p>
        <a href="#" class="get"> get started</a> </div>
      <img src="images/slider1.jpg" alt="slider" class="img-responsive"> </div>
    <div class="item">
      <div class="carousel-caption"> <span> massa  congue </span>
        <h3>Suspendisse </h3>
        <p><span>magna </span>, <span>efficitur </span> & <span>lectus </span></p>
        <a href="#" class="get"> get started</a> </div>
      <img src="images/slider1.jpg" alt="slider" class="img-responsive"> </div>
    <a class="mymouse"  href="#contact"><img class="mouse" src="images/mouse.png" alt="mouse"> </a> </div>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a>
  <div class="curve"> </div>
</div>
<!-- ABOUT -->
<section class="padding about myabout" id="about">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <figure> <img  src="images/about.jpg" alt=""> </figure>
      </div>
      <div class="col-md-6 col-sm-12">
        <h2>what we do?</h2>
        <h6>about us</h6>
        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
        <a class="get" href="#"> get started</a> </div>
      <div class="curve"> </div>
    </div>
  </div>
</section>
<!-- SERVICES -->
<section class="padding about services"  id="services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 myheading">
        <h2 class="text-center">Our Services</h2>
        <p class="col-sm-8 col-sm-offset-2 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
      </div>
      <div class="col-md-4 col-sm-6" >
        <figure> <span><img src="images/1.png" alt=""></span> <strong> web design</strong>
          <p>Sed a tellus a mauris volutpat sodales. Sed at mauris et lorem ullamcorper luctus et et diam. Sed a mauris quis nisl consectetur posuere eu aliquam neque. Suspendisse lacinia felis vitae ullamcorper venenatis.Nulla nec ante sed felis scelerisque blandit. </p>
        </figure>
      </div>
      <div class="col-md-4 col-sm-6">
        <figure> <span><img src="images/2.png" alt=""></span> <strong> Web Development</strong>
          <p>Vivamus sit amet iaculis urna. Aenean egestas urna vitae sem semper, et congue nulla consectetur. Mauris tempor turpis libero, semper ultrices nulla efficitur pharetra.Vestibulum tellus dui, facilisis nec turpis non, sagittis hendrerit nisl. </p>
        </figure>
      </div>
      <div class="col-md-4 col-sm-6">
        <figure> <span><img src="images/3.png" alt=""></span> <strong> Android App</strong>
          <p>Aenean volutpat lectus at nisi iaculis rutrum. Vivamus vestibulum libero enim, ut facilisis justo laoreet a. Nam sollicitudin consectetur sem, eget scelerisque velit sagittis nec. Morbi et aliquam lectus. Donec eget lorem nisl. </p>
        </figure>
      </div>
      <div class="col-md-4 col-sm-6">
        <figure> <span><img src="images/4.png" alt=""></span> <strong> Branding</strong>
          <p>Branding is one of the most important aspects of any business, large or small, retail or B2B. We provide an effective brand strategy that gives you a major edge in increasingly competitive markets and tells your customers what they can expert from your products and services.</p>
        </figure>
      </div>
      <div class="col-md-4 col-sm-6">
        <figure> <span><img src="images/5.png" alt=""></span> <strong> SEO</strong>
          <p>Backslash GH has an expertise in performing local Search Engine Optimization(SEO) that optimizes your business' online presence so that its web pages will be displayed by search engines when a user enters a local search for your products or services.</p>
        </figure>
      </div>
      <div class="col-md-4 col-sm-6">
        <figure> <span><img src="images/6.png" alt=""></span> <strong> Ecommerce</strong>
          <p> We develop fast and secure eCommerce websites that facilitate online transactions of goods and services through means of transfer of information and funds over the internet. </p>
        </figure>
      </div>
      <div class="curve"> </div>
    </div>
  </div>
</section>
<!-- steps -->
<section class="padding about services strategy "  id="setps">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 worksteps ste1 wow">
        <div class="col-sm-4 wow slideInLeft"> <b>01</b> <span>strategy</span> </div>
        <div class="col-sm-4 ">
          <div class="img_round"> <img src="images/step1.png" class="wow rollIn" data-wow-delay="1s"> <span class="divider wow fadeInUp" data-wow-delay="1.3s"></span> <img class="arrow wow fadeInDown" data-wow-delay="1.4s" src="images/right_arrow.png"></div>
        </div>
        <div class="col-sm-4 wow slideInRight">
          <p> Pellentesque at varius purus. Ut ex turpis, fermentum eget purus eget, suscipit dictum lorem. Sed iaculis dapibus metus ac cursus. Praesent et tincidunt dolor. Sed pretium nibh a erat scelerisque, ut vestibulum est tincidunt. Mauris suscipit lacus lectus. </p>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-sm-12 worksteps ste2 wow">
        <div class="col-sm-4 wow slideInLeft">
          <p> Pellentesque ligula quam, finibus eu urna eget, molestie bibendum nulla. Duis accumsan aliquam ipsum quis gravida. Nullam accumsan condimentum quam, in finibus quam aliquam vitae.</p>
        </div>
        <div class="col-sm-4">
          <div class="img_round"> <img src="images/step2.png" class="wow rollIn"  data-wow-delay="1s"> <img class="arrow wow fadeInDown" data-wow-delay="1.4s" src="images/left-arrow.png"><span class="divider wow fadeInUp" data-wow-delay="1.3s"></span> </div>
        </div>
        <div class="col-sm-4 wow slideInRight"><span>design</span> <b>02</b> </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-sm-12 worksteps ste3 wow">
        <div class="col-sm-4 wow slideInLeft"> <b>03</b> <span>develope</span> </div>
        <div class="col-sm-4">
          <div class="img_round"> <img src="images/step3.png" class="wow rollIn"><span class="divider wow fadeInUp" data-wow-delay="1.3s"></span> <img class="arrow wow fadeInDown" data-wow-delay="1.4s" src="images/right_arrow.png"></div>
        </div>
        <div class="col-sm-4 wow slideInRight">
          <p> Donec semper commodo leo, ut eleifend odio aliquet eget. Nullam euismod turpis ultrices, finibus enim non, luctus massa.</p>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-sm-12 worksteps ste4 wow">
        <div class="col-sm-4 wow slideInLeft">
          <p> We define your competition and target audience. Discover what is working in your online industry, then design your website accordingly.</p>
        </div>
        <div class="col-sm-4">
          <div class="img_round"> <img src="images/step4.png" class="wow rollIn wow fadeInDown" data-wow-delay="1.4s"> </div>
        </div>
        <div class="col-sm-4 wow slideInRight"><span>support</span> <b>04</b> </div>
        <div class="clearfix"></div>
      </div>
      <div class="curve"> </div>
    </div>
  </div>
</section>
<!-- recent work -->
<section class="padding about services work" id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 myheading">
        <h2 class="text-center"> recent works</h2>
        <p class="col-sm-8 col-sm-offset-2 text-center">Vestibulum tempus dignissim odio eget rhoncus. In luctus ornare enim et dignissim. Aenean enim eros, consectetur ac laoreet et, bibendum quis tortor.</p>
      </div>
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tab1">All</a></li>
        <li><a data-toggle="tab" href="#tab2"> web design</a></li>
        <li><a data-toggle="tab" href="#tab3"> graphic design</a></li>
        <li><a data-toggle="tab" href="#tab4"> Branding</a></li>
      </ul>
      <div class="tab-content">
        <div id="tab1" class="tab-pane fade in active">
          <div class="row">
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project1.jpg" class="swipebox" title="Recent Works"> <img src="images/project1.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project2.jpg" class="swipebox" title="Recent Works"> <img src="images/project2.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project4.jpg" class="swipebox" title="Recent Works"> <img src="images/project4.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
          </div>
        </div>
        <div id="tab2" class="tab-pane fade">
          <div class="row">
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project5.jpg" class="swipebox" title="Recent Works"> <img src="images/project5.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project4.jpg" class="swipebox" title="Recent Works"> <img src="images/project4.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project2.jpg" class="swipebox" title="Recent Works"> <img src="images/project2.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
          </div>
        </div>
        <div id="tab3" class="tab-pane fade">
          <div class="row">
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project3.jpg" class="swipebox" title="Recent Works"> <img src="images/project3.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project2.jpg" class="swipebox" title="Recent Works"> <img src="images/project2.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project1.jpg" class="swipebox" title="Recent Works"> <img src="images/project1.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
          </div>
        </div>
        <div id="tab4" class="tab-pane fade">
          <div class="row">
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project8.jpg" class="swipebox" title="Recent Works"> <img src="images/project8.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project7.jpg" class="swipebox" title="Recent Works"> <img src="images/project7.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
            <div class="col-sm-4">
              <figure> <span class="zoom"> <img src="images/zoom.png"></span> <a href="#" class="link"><img src="images/link.png"></a> <a href="images/project6.jpg" class="swipebox" title="Recent Works"> <img src="images/project6.jpg" alt="image" class="img-responsive"> </a> </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- our values -->
<section class="padding about services strategy values">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 myheading">
        <h2 class="text-center">Our values</h2>
      </div>
      <span class="divider"></span>
      <div  class="owl_1  owl-carousel owl-theme">
        <div class="item">
          <figure>
            <h1>Commitment and <span>Passion</span></h1>
            <p>We are committed in heart and mind and channel positive energy into our work to give you our very best beacuse we love what we do.</p>
          </figure>
        </div>
        <div class="item">
          <figure>
            <h1>Creativity and <span>Innovation </span></h1>
            <p>We come up with new and creative ideas in solving your IT related problems and using technology to achieve efficiency.</p>
          </figure>
        </div>
        <div class="item">
          <figure>
            <h1>Integrity and <span>Honesty </span></h1>
            <p>We are consistently open, honest, ethical and genuine. Our customers trust us to stick to our word.</p>
          </figure>
        </div>
        <div class="item">
          <figure>
            <h1>Quality and <span>Excellent Customer Service </span></h1>
            <p>We provide our customers with the best services and fully meet their requirements. We offer good customer care to enhance our relationship with our customers.</p>
          </figure>
        </div>
      </div>
      <div class="curve"> </div>
    </div>
  </div>
</section>
<!-- clients -->
<section class="padding about services strategy values clients">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 myheading">
        <h2 class="text-center"> clients Reviews</h2>
      </div>
      <span class="divider"></span>
      <div  class="owl_1  owl-carousel owl-theme">
        <div class="item">
          <figure>
            <div class="client-img"> <img src="images/user.svg"> </div>
            <b>austrila</b>
            <h3> valesanor</h3>
            <div class=" des">
              <p> good good good </p>
            </div>
          </figure>
        </div>
        <div class="item">
          <figure>
            <div class="client-img"> <img src="images/user.svg"> </div>
           
            <b>INDIA</b>
            <h3> manohar</h3>
            <div class=" des">
              <p> Imran did an excellent job in designing and building an awesome website. He was highly professional and easy to work with. He was reliable and gave us great work on schedule. Awasome Freelancer. Happy to Work Next Projects. Special Thanks to Imran. </p>
            </div>
          </figure>
        </div>
        <div class="item">
          <figure>
            <div class="client-img"> <img src="images/user.svg"> </div>
            <b>INDIA</b>
            <h3> Majeem mohammad </h3>
            <div class=" des">
              <p> Very Fast and Efficient. Always available to make changes and after service support was awesome. Follows instructions exactly. Can't get any better. </p>
            </div>
          </figure>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- get in touch -->
<section class="padding about myabout getintouch" id="contact">
  <div class="container">
    <div class="row">
      <div id="particles-js"></div>
      <div class="col-sm-6">
        <figure>
          <h2>Lets Get In Touch </h2>
          <h6>Contact Us</h6>
          <p>West is not just about graphic design; it's more than that. We offer integral communication services, and we're responsible for our process and results. </p>
          <strong>Our Contact details:</strong>
          <p><img src="images/mail.svg" alt=""> backslashgh@gmail.com</p>
          <p> <img src="images/skype.svg" alt=""> skypeusernamehere </p>
        </figure>
      </div>
      <div class="col-sm-6">
        <figure>
          <div class="row">
            <form method="post">
              <div class="form-group col-sm-6">
                <label> First Name :</label>
                <input type="text" class="form-control" required>
              </div>
              <div class="form-group col-sm-6">
                <label> Last  Name :</label>
                <input type="text" class="form-control" >
              </div>
              <div class="form-group col-sm-6">
                <label> E-mail  Address :</label>
                <input type="mail" class="form-control" >
              </div>
              <div class="form-group col-sm-6">
                <label> Phone Number :</label>
                <input type="tel" class="form-control" maxlength="10" >
              </div>
              <div class="form-group col-sm-12">
                <label> Message :</label>
                <textarea class="form-control" rows="5" > </textarea>
              </div>
              <div class="clearfix"></div>
              <div class="form-group col-sm-12 text-center">
                <button class="get"> submit request</button>
              </div>
            </form>
          </div>
        </figure>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-12 text-center">
        <p class="copyrights">© 2019 Backslash GH. All Rights Reserved.</p>
      </div>
    </div>
  </div>
</section>
<div class="count-particles"> <span class="js-count-particles"></span> </div>
<a href="#" id="gotop" style="display: inline;"> <i class="fa fa-angle-up"> </i> </a>
<script src="js/jquery-2.1.0.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/particles.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/stats.min.js"></script>
<script src="js/script.js"></script>
</body>

<!-- Mirrored from www.websolution.w3codemasters.in/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Jan 2019 03:53:52 GMT -->
</html>
